from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User

from accounts.models import UserAccount


class UserAccountForm(forms.ModelForm):
    first_name = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.CharField(max_length=200, widget=forms.EmailInput(attrs={'class': 'form-control'}))
    username = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=200, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    confirm_password = forms.CharField(max_length=200, widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = UserAccount
        fields = ('first_name', 'last_name', 'email', 'username', 'password', 'confirm_password')

    def save(self, commit=True):
        f = super(UserAccountForm, self).save(commit=False)
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        user = User.objects.create_user(username, email, password, first_name=first_name, last_name=last_name)
        f.user = user
        if commit:
            f.save()
        return f

    def clean_password(self):
        password = self.data.get('password')
        if len(password) < 8:
            raise forms.ValidationError('Password must be at least 8 chars long', code='password')
        return password

    def clean_confirm_password(self):
        password = self.data.get('password')
        password2 = self.data.get('confirm_password')

        if password != password2:
            raise forms.ValidationError("Password fields must match", code='confirm_password')
        return password2


class LoginForm(AuthenticationForm):
    username = forms.CharField(max_length=250, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=250, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
