from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from accounts.forms import UserAccountForm
from accounts.models import UserAccount
from library.forms import BookForm
from library.models import Book, BookBorrowed, BookRequested


class RegisterUser(CreateView):
    model = UserAccount
    form_class = UserAccountForm
    template_name = 'accounts/register_user.html'

    def get_success_url(self):
        messages.success(self.request, "You have successfully registered")
        return reverse('index')


def account(request):
    return render(request, 'accounts/index.html')


def settings(request):
    return render(request, 'accounts/settings.html')


# def changePassword(request):
#     return render(request, 'accounts/change_password.html')

class ChangePassword(UpdateView):
    model = User
    fields = ['password']
    template_name = 'accounts/change_password.html'

    def get_success_url(self):
        messages.success(self.request, "You have successfully changed your password")
        return reverse('profile', kwargs={'pk': int(self.request.user.pk)})


class UpdateEmail(UpdateView):
    model = User
    fields = ['email']
    template_name = 'accounts/update_email.html'

    def get_success_url(self):
        messages.success(self.request, "You have successfully updated your email")
        return reverse('profile', kwargs={'pk': int(self.request.user.pk)})


class UpdateUsername(UpdateView):
    model = User
    fields = ['username']
    template_name = 'accounts/update_username.html'

    def get_success_url(self):
        messages.success(self.request, "You have successfully updated your username")
        return reverse('profile', kwargs={'pk': int(self.request.user.pk)})


class DeleteAccount(DeleteView):
    model = UserAccount
    template_name = 'accounts/delete_account.html'
    success_url = reverse_lazy('index')


# def updateProfile(request):
#     return render(request, 'accounts/update_profile.html')

class UpdateProfile(UpdateView):
    model = UserAccount
    fields = ['address', 'phone_number', 'picture', 'biography']
    template_name = 'accounts/update_profile.html'

    def get_success_url(self):
        messages.success(self.request, "You have successfully updated your profile")
        return reverse('profile', kwargs={'pk': int(self.request.user.pk)})


def profile(request, pk):
    user = UserAccount.objects.get(pk=pk)
    all_books = user.book_set.all
    rented_books = Book.objects.filter(bookborrowed__user_id=user).distinct()
    purchased_books = Book.objects.filter(bookpurchased__user_id=user).distinct()
    context = {'user': user, 'allBooks': all_books, 'rented_books': rented_books,
               'purchased_books': purchased_books}
    return render(request, 'accounts/profile.html', context)


class QueueDetails(ListView):
    model = BookRequested
    template_name = 'accounts/queue_details.html'
    context_object_name = 'books'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(QueueDetails, self).get_context_data(**kwargs)
        user_books = Book.objects.filter(uploaded_by=self.request.user.useraccount)
        book = user_books.get(pk=self.kwargs.get('pk'))
        context['c'] = book.bookrequested_set.filter()
        context['book'] = book
        return context


class QueueList(ListView):
    model = BookRequested
    template_name = 'accounts/queue_list.html'
    context_object_name = 'books'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(QueueList, self).get_context_data(**kwargs)
        context['queuebooks'] = BookRequested.objects.filter(book__uploaded_by=self.request.user.useraccount)

        return context


def approve_request(request):
    book_pk = request.POST['book']
    user_pk = request.POST['user']
    book = Book.objects.get(pk=int(book_pk))
    user = UserAccount.objects.get(pk=int(user_pk))
    BookBorrowed.objects.create(book=book, user=user)
    BookRequested.objects.filter(user_id=int(user_pk), book_id=int(book_pk)).delete()
    send_mail(
        'Book granted',
        'Please ' + book.delivery_type + ' to get the book',
        'saliskamal@gmail.com',
        [book.uploaded_by.user.email, ],
        fail_silently=False,
    )
    messages.success(request, "Book approved")
    # context = {'user': user, 'book': book}
    return HttpResponseRedirect(reverse('profile', kwargs={'pk': int(request.user.pk)}))


class UpdateBook(UpdateView):
    model = Book
    form_class = BookForm
    template_name = 'accounts/edit_book.html'

    def get_success_url(self):
        messages.success(self.request, "You have successfully updated")
        return reverse('index')


class DeleteBook(DeleteView):
    model = Book
    template_name = 'accounts/delete_book.html'
    success_url = reverse_lazy('my-books')


class BookList(ListView):
    model = Book
    template_name = 'accounts/my_books.html'
    context_object_name = 'books'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(BookList, self).get_context_data(**kwargs)
        # context['my_books'] = Book.objects.filter(book__uploaded_by=self.request.user.useraccount)
        context['my_books'] = self.request.user.useraccount.book_set.all

        return context
