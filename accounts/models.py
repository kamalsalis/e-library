from django.contrib.auth.models import User
from django.db import models


# Create your models here.


class UserAccount(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # username, email, password, firstName, lastName
    address = models.CharField(max_length=500, null=True, blank=True)
    phone_number = models.CharField(max_length=11, null=True, blank=True)
    picture = models.ImageField(upload_to='users/images/', null=True, blank=True)
    biography = models.TextField(null=True, blank=True)
