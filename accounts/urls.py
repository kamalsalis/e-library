from django.urls import path

from django.contrib.auth.views import LoginView, LogoutView

from accounts import views
from accounts.forms import LoginForm

urlpatterns = [
    path('login', LoginView.as_view(template_name='accounts/login.html', authentication_form=LoginForm), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('register', views.RegisterUser.as_view(), name='register_user'),
    path('', views.account, name='account'),
    path('settings', views.settings, name='settings'),
    path('change-password/<int:pk>', views.ChangePassword.as_view(), name='change_password'),
    path('update-email/<int:pk>', views.UpdateEmail.as_view(), name='update_email'),
    path('update-username/<int:pk>', views.UpdateUsername.as_view(), name='update_username'),
    path('delete-account/<int:pk>', views.DeleteAccount.as_view(), name='delete_account'),
    path('profile/<int:pk>', views.profile, name='profile'),
    path('queue/<int:pk>', views.QueueDetails.as_view(), name='queue_details'),
    path('queuelist', views.QueueList.as_view(), name='queue_list'),
    path('approve', views.approve_request, name='approve'),
    path('edit_book/<int:pk>', views.UpdateBook.as_view(), name='update_book'),
    path('delete_book/<int:pk>', views.DeleteBook.as_view(), name='delete_book'),
    path('my-books', views.BookList.as_view(), name='my-books'),
    path('update-profile/<int:pk>', views.UpdateProfile.as_view(), name='update_profile')
]
