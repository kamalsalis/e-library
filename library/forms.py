from django import forms
from django.contrib.auth.models import User

from library.models import Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = (
            'name', 'about', 'author', 'genre', 'illustrator', 'publisher', 'language', 'date_published', 'page_number',
            'book_type', 'book_image', 'book_pdf', 'book_category', 'delivery_type')
        labels = {
            'book_pdf': 'PDF File'
        }
        exclude = ('date_added', 'is_available')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'about': forms.Textarea(attrs={'class': 'form-control'}),
            'author': forms.TextInput(attrs={'class': 'form-control'}),
            'genre': forms.TextInput(attrs={'class': 'form-control'}),
            'illustrator': forms.TextInput(attrs={'class': 'form-control'}),
            'publisher': forms.TextInput(attrs={'class': 'form-control'}),
            'language': forms.TextInput(attrs={'class': 'form-control'}),
            'date_published': forms.DateInput(attrs={'class': 'form-control'}),
            'page_number': forms.TextInput(attrs={'class': 'form-control'}),
            'book_type': forms.Select(attrs={'class': 'form-control'}),
            'bookImage': forms.FileInput(attrs={'class': 'form-control'}),
            'book_pdf': forms.FileInput(attrs={'class': 'form-control'}),
            'book_category': forms.Select(attrs={'class': 'form-control'}),
            'delivery_type': forms.Select(attrs={'class': 'form-control'}),

        }
