# Generated by Django 2.1.1 on 2018-11-26 09:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('about', models.TextField()),
                ('illustrator', models.CharField(max_length=200)),
                ('date_published', models.DateField()),
                ('genre', models.CharField(max_length=200)),
                ('publisher', models.CharField(max_length=200)),
                ('book_image', models.ImageField(upload_to='books/images/')),
                ('book_pdf', models.FileField(blank=True, null=True, upload_to='books/pdfs')),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('book_type', models.CharField(choices=[('Ebook', 'Ebook'), ('Printed book', 'Printed book')], max_length=200)),
                ('author', models.CharField(max_length=200)),
                ('books_we_love', models.BooleanField(null=True)),
                ('book_category', models.CharField(blank=True, choices=[('For Sale', 'For Sale'), ('For Rent', 'For Rent')], max_length=200, null=True)),
                ('delivery_type', models.CharField(blank=True, choices=[('Meet at Mosque', 'Meet at Mosque'), ('Meet at Home', 'Meet at Home')], max_length=200, null=True)),
                ('page_number', models.IntegerField()),
                ('language', models.CharField(max_length=200)),
                ('is_available', models.BooleanField(null=True)),
                ('uploaded_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.UserAccount')),
            ],
        ),
        migrations.CreateModel(
            name='BookBorrowed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_Borrowed', models.DateTimeField(auto_now_add=True)),
                ('date_Returned', models.DateTimeField(auto_now_add=True)),
                ('returned', models.BooleanField(default=False)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='library.Book')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.UserAccount')),
            ],
            options={
                'ordering': ['-date_Borrowed'],
            },
        ),
        migrations.CreateModel(
            name='BookPurchased',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_Borrowed', models.DateTimeField(auto_now_add=True)),
                ('date_Returned', models.DateTimeField(auto_now=True)),
                ('returned', models.BooleanField(default=False)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='library.Book')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.UserAccount')),
            ],
        ),
        migrations.CreateModel(
            name='BookRequested',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_requested', models.DateTimeField(auto_now_add=True)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='library.Book')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.UserAccount')),
            ],
            options={
                'ordering': ['-date_requested'],
            },
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('review', models.TextField()),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='library.Book')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.UserAccount')),
            ],
        ),
    ]
