from random import randint

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.views.generic import DetailView, CreateView, ListView

from accounts.models import UserAccount
from library.forms import BookForm
from library.models import Book, Review, BookBorrowed, BookPurchased, BookRequested


def index(request):
    books = Book.objects.all()

    context = {'books': books, 'buy': Book.objects.filter(book_category='For Sale'),
               'genre': Book.objects.all().values_list('genre').distinct(),
               'borrow': Book.objects.filter(book_category='For Rent'),
               'ebooks': Book.objects.filter(book_type='Ebook'),
               }

    return render(request, 'library/index.html', context)


class BookDetails(DetailView):
    model = Book
    template_name = 'library/bookdetails.html'
    context_object_name = 'book'

    def get_context_data(self, **kwargs):
        context = super(BookDetails, self).get_context_data(**kwargs)
        context['url'] = reverse('book_details', kwargs={'pk': self.kwargs.get('pk')})
        context['maylike'] = Book.objects.filter(genre=self.object.genre).exclude(pk=self.kwargs.get('pk'))[0:2]
        return context


@login_required
def add_review(request):
    if request.method == 'POST':
        bookid = request.POST['bookid']
        review = request.POST['review']
        book = Book.objects.get(pk=int(bookid))
        Review.objects.create(book=book, review=review, user=request.user.useraccount)
        messages.success(request, "Review posted")
        return HttpResponseRedirect(reverse('book_details', kwargs={'pk': int(bookid)}))
    else:
        return render(request, 'library/bookdetails.html')


@method_decorator(login_required, name='dispatch')
class AddBook(CreateView):
    model = Book
    template_name = 'library/addbook.html'
    form_class = BookForm

    def get_success_url(self):
        messages.success(self.request, "Book added successfully")
        return reverse('index')

    def get_context_data(self, **kwargs):
        context = super(AddBook, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        form.instance.uploaded_by = self.request.user.useraccount
        return super().form_valid(form)


class BookList(ListView):
    model = Book
    template_name = 'library/all_books.html'
    context_object_name = 'books'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(BookList, self).get_context_data(**kwargs)
        context['rents'] = Book.objects.filter(book_category='For Rent')
        context['ebooks'] = Book.objects.filter(book_type='Ebook')
        context['buys'] = Book.objects.filter(book_category='For Sale')
        return context



@login_required
def borrow_page(request):
    bookid = request.POST['bookid']
    book = Book.objects.get(pk=int(bookid))
    user = request.user.useraccount

    BookRequested.objects.create(book=book, user=user)
    # BookBorrowed.objects.all().delete()

    send_mail(
        'Book request',
        user.user.first_name + ' ' + user.user.last_name + ' requested to borrow your book. Kindly visit your profile to approve request.',
        'saliskamal@gmail.com',
        [book.uploaded_by.user.email, 'saliskamal@hotmail.co.uk'],
        fail_silently=False,
    )
    messages.success(request, "Book requested")
    context = {'user': user, 'book': book}
    return render(request, 'library/borrow_page.html', context)


@login_required
def buy_page(request):
    bookid = request.POST['bookid']
    book = Book.objects.get(pk=int(bookid))
    user = request.user.useraccount

    # BookPurchased.objects.create(book=book, user=user)
    # BookPurchased.objects.all().delete()
    # send_mail(
    #     'Book request',
    #     user.user.first_name + ' ' + user.user.last_name + ' requested to borrow your book. Follow the link to accept http://localhost:8080/account/queuelist',
    #     'mailer@primaldux.com',
    #     [book.uploaded_by.user.email, 'saliskamal@hotmail.co.uk'],
    #     fail_silently=False,
    # )
    # send_mail(
    #     'Book request',
    #     user.user.first_name + ' ' + user.user.last_name + ' requested to borrow your book. Follow the link to accept http://localhost:8080/account/queuelist',
    #     'saliskamal@gmail.com',
    #     [book.uploaded_by.user.email, ],
    #     fail_silently=False,
    # )
    # messages.success(request, "Book requested")
    context = {'user': user, 'book': book}
    return render(request, 'library/buy_page.html', context)


class GenreList(ListView):
    model = Book
    template_name = 'library/genre_search.html'
    context_object_name = 'books'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(GenreList, self).get_context_data(**kwargs)
        # context['my_books'] = Book.objects.filter(book__uploaded_by=self.request.user.useraccount)
        context['genre_books'] = Book.objects.filter(genre=self.kwargs.get('genre'))

        return context


def search(request):
    if request.method == 'POST':
        book = request.POST['search-input']
        books = Book.objects.filter(name__contains=book)

        context = {'books': books}

        return render(request, 'library/search_result.html', context)
    # else:
    #     HttpResponse('Not working')
