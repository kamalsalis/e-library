from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('book/<int:pk>', views.BookDetails.as_view(), name='book_details'),
    path('addbook', views.AddBook.as_view(), name='add_book'),
    path('add-review', views.add_review, name='addreview'),
    path('all-books', views.BookList.as_view(), name='book_list'),
    path('borrow', views.borrow_page, name='borrow_page'),
    path('buy', views.buy_page, name='buy_page'),
    path('genre/<str:genre>', views.GenreList.as_view(), name='genre_list'),
    path('search', views.search, name='search'),

]
