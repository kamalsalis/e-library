from django.db import models

# Create your models here.
from accounts.models import UserAccount


class Book(models.Model):
    name = models.CharField(max_length=200)
    about = models.TextField()
    illustrator = models.CharField(max_length=200)
    date_published = models.DateField()
    genre = models.CharField(max_length=200)
    publisher = models.CharField(max_length=200)
    book_image = models.ImageField(upload_to='books/images/')
    book_pdf = models.FileField(upload_to='books/pdfs', blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    book_type = models.CharField(max_length=200, choices=(
        ('Ebook', 'Ebook'),
        ('Printed book', 'Printed book'),
    ))
    author = models.CharField(max_length=200)
    books_we_love = models.BooleanField(null=True)
    book_category = models.CharField(max_length=200, choices=(
        ('For Sale', 'For Sale'),
        ('For Rent', 'For Rent'),
    ), null=True, blank=True)
    delivery_type = models.CharField(max_length=200, choices=(
        ('Meet at Mosque', 'Meet at Mosque'),
        ('Meet at Home', 'Meet at Home')
    ), null=True, blank=True)
    page_number = models.IntegerField()
    language = models.CharField(max_length=200)
    uploaded_by = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    is_available = models.BooleanField(null=True)

    def __str__(self):
        return self.name


class Review(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    review = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.book)

class BookRequested(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    date_requested = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user)

    class Meta:
        ordering = ['-date_requested']

class BookBorrowed(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    date_Borrowed = models.DateTimeField(auto_now_add=True)
    date_Returned = models.DateTimeField(auto_now_add=True)
    returned = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user)

    class Meta:
        ordering = ['-date_Borrowed']


class BookPurchased(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    date_Borrowed = models.DateTimeField(auto_now_add=True)
    date_Returned = models.DateTimeField(auto_now=True)
    returned = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user)
