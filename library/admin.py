from django.contrib import admin

from library.models import Review, BookPurchased, BookBorrowed
from .models import Book

admin.site.register(Book)
admin.site.register(Review)
admin.site.register(BookPurchased)
admin.site.register(BookBorrowed)
