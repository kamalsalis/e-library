from django import template

from random import choice

from library.models import Book

register = template.Library()


@register.filter
def get_genre_count(genre):
    return Book.objects.filter(genre=genre).count()


@register.inclusion_tag('base/random_button.html', takes_context=True)
def get_button(context):
    books = Book.objects.all()
    book = choice(books) if books else None
    return {'pk': book.pk if book else None}
